//Robert Ye   (rye3)
//Peter Goh   (pgoh4)
//Michael Li  (mli114)


#include "Chess.h"
#include <cmath>

//assignment operator
void Chess::operator=(const Chess& orig){
  board = orig.board;
  is_white_turn = orig.is_white_turn;
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
	// Add the pawns
	for (int i = 0; i < 8; i++) {
		board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
		board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
	}

	// Add the rooks
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}


// Attempts to make a move. If successful, the move is made and the turn is switched white<->black
bool Chess::make_move(std::pair<char, char> start, std::pair<char, char> end) {
  //make temporary copy of the Board
  Board tempBoard(board);
  
  if(!(good_move(start, end))) {
    return false;
  }

  //Make the specified move
  board.remove_piece(end);
  board.add_piece(end, board(start)->to_ascii());
  board.remove_piece(start);

  if((board(end)->to_ascii() == 'P') && (end.second == '8')) {
    board.remove_piece(end);
    board.add_piece(end, 'Q');
  }
  else if((board(end)->to_ascii() == 'p') && (end.second == '1')) {
    board.remove_piece(end);
    board.add_piece(end, 'q');
  }  
  
  //Check to see if after the specified move, we are in check
  if(in_check(is_white_turn)) {
    board = tempBoard;
    return false;
  }

  is_white_turn = !(is_white_turn);
  return true;
}


bool Chess::make_move_o(std::pair<char, char> start, std::pair<char, char> end) {
  //make temporary copy of the Board
  Board tempBoard(board);
  
  if(!(good_move_o(start, end))) {
    return false;
  }

  //Make the specified move
  board.remove_piece(end);
  board.add_piece(end, board(start)->to_ascii());
  board.remove_piece(start);

  if((board(end)->to_ascii() == 'P') && (end.second == '8')) {
    board.remove_piece(end);
    board.add_piece(end, 'Q');
  }
  else if((board(end)->to_ascii() == 'p') && (end.second == '1')) {
    board.remove_piece(end);
    board.add_piece(end, 'q');
  }  
  
  //Check to see if after the specified move, we are in check
  if(in_check(is_white_turn)) {
    board = tempBoard;
    return false;
  }

  is_white_turn = !(is_white_turn);
  return true;
}


bool Chess::good_move(std::pair<char, char> start, std::pair<char, char> end) const {

  // check if start points to null
  if (!board(start)) {
    std::cerr << "Bad Move: No piece at specified coordinates" << std::endl;
    return false;
  }
  
  // check validity of start and end coordinates
  if ((start.first < 'A') || (start.first > 'H') || (start.second < '1') || (start.second > '8') || (end.first < 'A') || (end.first > 'H') || (end.second < '1') || (end.second > '8')) {
    std::cerr << "Bad Move: Out of range" << std::endl;
    return false;
  }

  if (start == end) {
    std::cerr << "Bad Move: Ending where you start" << std::endl;
    return false;
  }

  // check if there is a piece, but capture does not pass while move does
  if (board(end) && board(start)->legal_move_shape(start, end) && !board(start)->legal_capture_shape(start, end)) {
    std::cerr << "Bad Move: Piece in the way" << std::endl;
    return false;
  }
  
  //Check if piece you are moving is your piece
  if (board(start)->is_white() != is_white_turn) {
    std::cerr << "Bad Move: Not your piece" << std::endl;
    return false;
  }
    
  // check if neither move nor capture tests pass
  if (!board(start)->legal_move_shape(start, end) && !board(start)->legal_capture_shape(start, end)) {
    std::cerr << "Bad Move: Invalid move" << std::endl;
    return false;
  }
  
  // check if there are bad end values to where we are moving (same color at the end as what we are moving)
  if(board(end) && (board(end)->is_white() == board(start)->is_white())) {
    std::cerr << "Bad Move: Capture your own piece" << std::endl;
    return false;
  }

  // check if capture passes and move does not, but there is nothing to capture
  if (board(start)->legal_capture_shape(start, end) && !board(start)->legal_move_shape(start, end) && !board(end)) {
    std::cerr << "Bad Move: Invalid move" << std::endl;
    return false;
  }

  // check if something is in the way of the move
  std::pair<char, char> temp = start;
  if (board(start)->legal_move_shape(start, end) || (board(start)->legal_capture_shape(start, end))) {
    // vertical movement
    if (end.first == start.first) {
      // upwards
      if (end.second > start.second) {
	for (int i = 0; i < (end.second - start.second - 1); i++) {
	  temp.second++;
	  if (board(temp)) {
	    std::cerr << "Bad Move: Piece in the way" << std::endl;
	    return false;
	  }
	}
      }
      // downwards
      else {
	for (int i = 0; i < (start.second - end.second - 1); i++) {
          temp.second--;
          if (board(temp)) {
	    std::cerr << "Bad Move: Piece in the way" << std::endl;
            return false;
          }
        }
      }
    }
    // horizontal movement
    else if (end.second == start.second) {
      // rightwards
      if (end.first > start.first) {
	for (int i = 0; i < (end.first - start.first - 1); i++) {
          temp.first++;
          if (board(temp)) {
	    std::cerr << "Bad Move: Piece in the way" << std::endl;
            return false;
          }
        }
      }
      // leftwards
      else {
	for (int i = 0; i < (start.first - end.first - 1); i++) {
          temp.first--;
          if (board(temp)) {
	    std::cerr << "Bad Move: Piece in the way" << std::endl;
            return false;
          }
        }
      }
    }
    
    // diagonal
    else if (std::abs((int)(end.second - start.second)) == std::abs(((int)(end.first - start.first)))) {
      // upwards
      if (end.second > start.second) {
	// rightwards
	if (end.first > start.first) {
	  for (int i = 0; i < (end.first - start.first - 1); i++) {
	    temp.first++;
	    temp.second++;
	    if (board(temp)) {
	      std::cerr << "Bad Move: Piece in the way" << std::endl;
	      return false;
	    }
	  }
	}
	// leftwards
	else {
	  for (int i = 0; i < (start.first - end.first - 1); i++) {
            temp.first--;
            temp.second++;
            if (board(temp)) {
	      std::cerr << "Bad Move: Piece in the way" << std::endl;
              return false;
            }
          }

	}
      }
      // downwards
      else {
	// rightwards
	if (end.first > start.first) {
	  for (int i = 0; i < (end.first - start.first - 1); i++) {
            temp.first++;
            temp.second--;
            if (board(temp)) {
	      std::cerr << "Bad Move: Piece in the way" << std::endl;
              return false;
            }
          }
	  
	}
	// leftwards
	else {
	  for (int i = 0; i < (start.first - end.first - 1); i++) {
            temp.first--;
            temp.second--;
            if (board(temp)) {
	      std::cerr << "Bad Move: Piece in the way" << std::endl;
              return false;
            }
          }
	  
	}
      }
    }
    // else, none of the above, so Mystery or Knight which passes over pieces
  }
  return true;
}


bool Chess::good_move_o(std::pair<char, char> start, std::pair<char, char> end) const {

  // check if start points to null
  if (!board(start)) {
    return false;
  }
  
  // check validity of start and end coordinates
  if ((start.first < 'A') || (start.first > 'H') || (start.second < '1') || (start.second > '8') || (end.first < 'A') || (end.first > 'H') || (end.second < '1') || (end.second > '8')) {

    return false;
  }

  if (start == end) {
    return false;
  }

  // check if there is a piece, but capture does not pass while move does
  if (board(end) && board(start)->legal_move_shape(start, end) && !board(start)->legal_capture_shape(start, end)) {
    return false;
  }
  
  //Check if piece you are moving is your piece
  if (board(start)->is_white() != is_white_turn) {
    return false;
  }
    
  // check if neither move nor capture tests pass
  if (!board(start)->legal_move_shape(start, end) && !board(start)->legal_capture_shape(start, end)) {
    return false;
  }
  
  // check if there are bad end values to where we are moving (same color at the end as what we are moving)
  if(board(end) && (board(end)->is_white() == board(start)->is_white())) {
    return false;
  }

  // check if capture passes and move does not, but there is nothing to capture
  if (board(start)->legal_capture_shape(start, end) && !board(start)->legal_move_shape(start, end) && !board(end)) {
    return false;
  }

  // check if something is in the way of the move
  std::pair<char, char> temp = start;
  if (board(start)->legal_move_shape(start, end) || (board(start)->legal_capture_shape(start, end))) {
    // vertical movement
    if (end.first == start.first) {
      // upwards
      if (end.second > start.second) {
	for (int i = 0; i < (end.second - start.second - 1); i++) {
	  temp.second++;
	  if (board(temp)) {
	    return false;
	  }
	}
      }
      // downwards
      else {
	for (int i = 0; i < (start.second - end.second - 1); i++) {
          temp.second--;
          if (board(temp)) {
            return false;
          }
        }
      }
    }
    // horizontal movement
    else if (end.second == start.second) {
      // rightwards
      if (end.first > start.first) {
	for (int i = 0; i < (end.first - start.first - 1); i++) {
          temp.first++;
          if (board(temp)) {
            return false;
          }
        }
      }
      // leftwards
      else {
	for (int i = 0; i < (start.first - end.first - 1); i++) {
          temp.first--;
          if (board(temp)) {
            return false;
          }
        }
      }
    }
    
    // diagonal
    else if (std::abs((int)(end.second - start.second)) == std::abs(((int)(end.first - start.first)))) {
      // upwards
      if (end.second > start.second) {
	// rightwards
	if (end.first > start.first) {
	  for (int i = 0; i < (end.first - start.first - 1); i++) {
	    temp.first++;
	    temp.second++;
	    if (board(temp)) {
	      return false;
	    }
	  }
	}
	// leftwards
	else {
	  for (int i = 0; i < (start.first - end.first - 1); i++) {
            temp.first--;
            temp.second++;
            if (board(temp)) {
              return false;
            }
          }

	}
      }
      // downwards
      else {
	// rightwards
	if (end.first > start.first) {
	  for (int i = 0; i < (end.first - start.first - 1); i++) {
            temp.first++;
            temp.second--;
            if (board(temp)) {
              return false;
            }
          }
	  
	}
	// leftwards
	else {
	  for (int i = 0; i < (start.first - end.first - 1); i++) {
            temp.first--;
            temp.second--;
            if (board(temp)) {
              return false;
            }
          }
	  
	}
      }
    }
    // else, none of the above, so Mystery or Knight which passes over pieces
  }
  return true;
}


bool Chess::in_check(bool white) const {
  //make a copy of the current board
  Chess tempChess(*this);

  //find position of our king
  std::pair<char, char> position;
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      if (tempChess.board(std::pair<char, char>(c, r)) && tempChess.board(std::pair<char, char>(c, r))->is_white() == white) {
	if (tempChess.board(std::pair<char, char>(c, r))->to_ascii() == 'k' || tempChess.board(std::pair<char, char>(c, r))->to_ascii() == 'K') {
	  position.first = c;
	  position.second = r;
	}
      }
    }//inner for
  }//outer for
  
  //invert turn color  
  tempChess.is_white_turn = !is_white_turn;
  
  // loop through the board to find possible moves which do not result in check
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      if (tempChess.good_move_o(std::pair<char, char>(c, r), position)) {
	return true;
      }
    }
  }
  return false;
}


bool Chess::in_mate(bool white) const {
  //check if currently in check
  if(!in_check(white)){ 
    return false;
  }

  //make a copy of our chess
  Chess tempChess(*this);
  
  std::pair<char, char> position;
  //check that there are no possible moves without putting oneself in check
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      if (board(std::pair<char, char>(c, r)) && board(std::pair<char, char>(c, r))->is_white() == white) {
	position.first = c;
	position.second = r;
	for (char row = '8'; row >= '1'; row--) {
	  for (char col = 'A'; col <= 'H'; col++) {
	    if(tempChess.make_move_o(position, std::pair<char, char>(col, row))){
	      return false;
	    }
	  }
	}
      }
    }
  }
  return true;
}


bool Chess::in_stalemate(bool white) const {
  //check if not in check
  if(!in_check(white)){ 
    
    Chess tempChess(*this);
    
    std::pair<char, char> position;
    // check that there are no possible moves without putting oneself in check
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	if (board(std::pair<char, char>(c, r)) && board(std::pair<char, char>(c, r))->is_white() == white) {
	  position.first = c;
	  position.second = r;
	  for (char row = '8'; row >= '1'; row--) {
	    for (char col = 'A'; col <= 'H'; col++) {
	      if(tempChess.make_move_o(position, std::pair<char, char>(col, row))){
		return false;
	      }
	    }
	  }
	}
      }
    }
    return true;
  }
  else{
    return false;
  }
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
	// Write the board out and then either the character 'w' or the character 'b',
	// depending on whose turn it is
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}


std::istream& operator>> (std::istream& is, Chess& chess) {
  char cha;
  Board tempBoard;
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      is >> cha;
      tempBoard.add_piece(std::pair<char, char>(c, r), cha);
    }
  }
  is >> cha;
  chess.set_white_turn(cha == 'w');
  chess.set_board(tempBoard);
  return is;
}
