//Robert Ye   (rye3)
//Peter Goh   (pgoh4)
//Michael Li  (mli114)


#include "Bishop.h"
#include <cmath>

bool Bishop::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // valid coordinates already checked in make_move                                                                                                                                                          
  // start == end already checked in make_move                                                                                                                                                             
  // checking for diagonal movement
  if ((std::abs((int)end.second - (int)start.second)) != (std::abs((int)end.first - (int)start.first))){
    return false;
  }
							  
  return true;

}
