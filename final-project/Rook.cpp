//Robert Ye   (rye3)
//Peter Goh   (pgoh4)
//Michael Li  (mli114)


#include "Rook.h"

bool Rook::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  if ((end.first != start.first) && (end.second != start.second)) {
    return false;
  }
  return true;
}
