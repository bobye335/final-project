//Robert Ye   (rye3)
//Peter Goh   (pgoh4)
//Michael Li  (mli114)


#include "Pawn.h"
#include <cmath>

bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {

  // valid coordinates already checked in make_move

  // start == end already checked in make_move
  
  // checking for vertical movement
  if (start.first != end.first) {
    return false;
  }
  
  // distance moved checking
  if (is_white()) { // if white
    if (start.second == '2') { // if first move it can move 1 or 2
      if (((end.second - start.second) < 1) || ((end.second - start.second) > 2)) {
	return false;
      }
    }
    else { // if not first move it can move only 1
      if ((end.second - start.second) != 1) { // 
	return false;
      }
    }
  }
  else { // if black
    if (start.second == '7') { // if first move it can move 1 or 2
      if (((start.second - end.second) < 1) || ((start.second - end.second) > 2)) {
	return false;
      }
    }
    else { // if not first move it can move only 1
      if ((start.second - end.second) != 1) {
        return false;
      }
    }
  }

  // if it passes all above tests, return true
  return true;
}

bool Pawn::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {

  // valid coordinates already checked in make_move

  // start == end already checked in make_move

  // checking for diagonal movement
  if (std::abs(start.first - end.first) != std::abs(start.second - end.second)) {
    return false;
  }
  
  // checking for right direction of movement and only vertical movement of 1 up for white and 1 down for black
  // - we don't need to check horizontal movement because we know it's magnitude is the same as vertical movement already
  if (is_white()) { // if white
    if ((end.second - start.second) != 1) { // if vertical movement isn't up 1 
      return false;                         
    }
  }
  else { // if black
    if ((start.second - end.second) != 1) { // if vertical movement isn't down 1
      return false;
    }
  }

  // if it passes all above tests, return true
  return true;
}
