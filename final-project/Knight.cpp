//Robert Ye   (rye3)
//Peter Goh   (pgoh4)
//Michael Li  (mli114)


#include "Knight.h"
#include <cmath>

bool Knight::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // valid coordinates already checked in make_move

  // start == end already checked in make_move

  // checking for distance of root(5)
  // maybe ask about double accuracy in our calculation being != root(5), will it ever actually ever equal root(5)?
  if (std::sqrt(std::pow(((double)start.first - (double)end.first), 2) + std::pow(((double)start.second - (double)end.second), 2)) != std::sqrt(5)) {
    return false;
  }
  return true;
}
