//Robert Ye   (rye3)
//Peter Goh   (pgoh4)
//Michael Li  (mli114)


#include "King.h"
#include <cmath>

bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // maybe ask about double accuracy and if it will effect this calculation being < root(2)
  if(std::sqrt(std::pow(((double)start.first) - ((double)end.first), 2) + std::pow(((double)start.second - (double)end.second), 2)) > std::sqrt(2)) {
    return false;
  }
  return true;
}
