//Robert Ye   (rye3)
//Peter Goh   (pgoh4)
//Michael Li  (mli114)


#include "Queen.h"
#include <cmath>

bool Queen::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // valid coordinates already checked in make_move

  // start == end already checked in make_move

  // checking for straight line movement
  if ((end.first != start.first) && (end.second != start.second) && (std::abs((int)(end.second - start.second))) != std::abs(((int)(end.first - start.first)))) {
    return false;
  }

  return true;
}
