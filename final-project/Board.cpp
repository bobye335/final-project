//Robert Ye   (rye3)
//Peter Goh   (pgoh4)
//Michael Li  (mli114)


#include <iostream>
#include <utility>
#include <map>
#include "Board.h"
#include "CreatePiece.h"
#include "Terminal.h"

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board(){}


Board::Board(const Board& orig){
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      if(orig(std::pair<char, char>(c, r))) {
	add_piece(std::pair<char, char>(c, r), orig(std::pair<char, char>(c, r))->to_ascii());
      }
    }
  }
}


Board::~Board() {  
  for(std::map<std::pair<char, char>, Piece*>::iterator itr = occ.begin(); itr != occ.end(); itr++) {
    delete itr->second;
  }
}


const Piece* Board::operator()(std::pair<char, char> position) const {
  std::map<std::pair<char, char>, Piece*>::const_iterator itr = occ.find(position);
  if (itr != occ.end()) {
    return (itr->second);
  }
  else {
    return nullptr;
  }
}


void Board::operator=(const Board& orig){
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      remove_piece(std::pair<char, char>(c, r));
      if(orig(std::pair<char, char>(c, r))) {
	add_piece(std::pair<char, char>(c, r), orig(std::pair<char, char>(c, r))->to_ascii());
      }
    }
  }
}


bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
  // check position is on board
  if ((position.first < 'A') || (position.first > 'H') || (position.second < '1') || (position.second > '8') ) {
    std::cerr << "Bad Coordinates" << std::endl;
    return false;
  }

  // check if the position is occupied
  if (occ[position]) {
    return false;
  }

  // assign occ[position] the new piece
  occ[position] = create_piece(piece_designator);

  // if occ[position] is still NULL, return false
  if (!occ[position]) {
    return false;
  }
  
  return true;
}


bool Board::remove_piece(std::pair<char, char> position){
  // check valid position
  if ((position.first < 'A') || (position.first > 'H') || (position.second < '1') || (position.second > '8') ) {
    return false;
  }
  
  if (occ.find(position) != occ.end()) {
    delete (occ.find(position))->second;
    occ.erase(position);
    return true;
  }
  
  return true;  
}


bool Board::has_valid_kings() const {
  //counters for black and white
  int bcount = 0;
  int wcount = 0;

  //iterate through board and check for black/white kings
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {

      const Piece* piece = (*this)(std::pair<char, char>(c, r));

      if (!piece) {
	continue;
      }
      
      if (piece->to_ascii() == 'k') {
	bcount++;
      }
      if (piece->to_ascii() == 'K') {
	wcount++;
      }
    }
  }

  // check if bcount and wcount are both 1
  if (bcount == 1 && wcount == 1) {
    return true;
  }
  return false;
}


void Board::display() const {
  int counter = 0;
  int row = 8;
  std::string cols = "  ABCDEFGH";

  for(char r = '8'; r >= '1'; r--) {
    std::cout << row << " ";
    row--;
    for(char c = 'A'; c <= 'H'; c++) {
      if (counter % 2 == 0) {
	Terminal::color_bg(Terminal::CYAN);	
      }
      else {
	Terminal::color_bg(Terminal::YELLOW);
      }
      const Piece* piece = (*this)(std::pair<char, char>(c, r));
      if (piece) {
	if (piece->is_white()) { // white color
	  Terminal::color_fg(true, Terminal::WHITE);
	}
	else { // black color
	  Terminal::color_fg(false, Terminal::BLACK);
	}
	std::cout << piece->to_ascii();
      }
      else { // QUESTION are the dashes necessary?
	Terminal::color_fg(true, Terminal::RED);
	std::cout << ' ';
      }
      Terminal::set_default();
      counter++;
    }
    counter++;
    std::cout << std::endl;
  }
  std::cout << std::endl << cols << std::endl;
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const Board& board) {
	for(char r = '8'; r >= '1'; r--) {
		for(char c = 'A'; c <= 'H'; c++) {
			const Piece* piece = board(std::pair<char, char>(c, r));
			if (piece) {
				os << piece->to_ascii();
			} else {
			  os << '-';
			}
		}
		os << std::endl;
	}
	return os;
}

